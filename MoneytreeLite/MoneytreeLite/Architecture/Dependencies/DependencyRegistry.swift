import UIKit

class DependencyRegistry {
    static var shared = DependencyRegistry()

    private var navigationCoordinator = NavigationCoordinator()
    private var networkInteractor: NetworkInteractor = MockNetworkInteraction() //using mock until endpoints are finished // NetworkInteraction()
    private var dataInteractor: DataInteractor = DataInteraction()
    
    private var modelInteractor: ModelInteractor { //singleton
        return ModelInteraction(networkInteractor: networkInteractor, dataInteractor: dataInteractor)
    }
    
    private var mainPresenter: MainPresenter {
        return MainPresenter(interactor: modelInteractor, accountsGrouper: accountsGrouper)
    }
    
    private var accountsGrouper: AccountsGrouper = AccountsGroupingHelper()
    
    func createDetailViewController(with account: Account) -> DetailViewController {
        return DetailViewController.newInstance(with: account)
    }
    
    //this is the initial view controller, so we need to do some extra setup
    func prepare(_ mvc: MainViewController) {
        navigationCoordinator.registerInitialNavigationController(mvc)
        
        let presenter = mainPresenter
        mvc.inject(presenter: presenter, navigationCoordinator: navigationCoordinator)
    }
    
    func prepare(_ vc: DetailViewController, with account: Account) {
        let presenter = DetailPresenter(account: account, modelInteractor: modelInteractor)
        vc.inject(presenter: presenter, navigationCoordinator: navigationCoordinator)
    }
}
