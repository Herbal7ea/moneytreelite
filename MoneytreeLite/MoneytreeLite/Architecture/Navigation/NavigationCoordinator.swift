import UIKit

class NavigationCoordinator {
    private var navigationController: UINavigationController!
    
    func registerInitialNavigationController(_ mvc: MainViewController){
        self.navigationController = mvc.navigationController
    }
    
    func rowTappedForAccount(_ account: Account) {
        let vc = DependencyRegistry.shared.createDetailViewController(with: account)
        navigationController.pushViewController(vc, animated: true)
    }
    
    
    func detailVCDismissed() {
        //handle back event if need be
    }
}
