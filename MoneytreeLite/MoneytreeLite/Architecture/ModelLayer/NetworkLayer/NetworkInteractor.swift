import Foundation

protocol NetworkInteractor {
    func loadAccounts(finished: @escaping AccountsResultClosure)
    func loadTransactions(id: Int, finished: @escaping TransactionsResultClosure)
}

class NetworkInteraction: NetworkInteractor {
    func loadAccounts(finished: @escaping AccountsResultClosure) {
        fatalError("Not Implemented")
    }
    
    func loadTransactions(id: Int, finished: @escaping TransactionsResultClosure) {
        fatalError("Not Implemented")
    }
}
