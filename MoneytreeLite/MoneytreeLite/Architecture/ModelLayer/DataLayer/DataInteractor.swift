import Foundation
import RxSwift
import RxRelay

protocol DataInteractor {
    
    var accountsRelay: BehaviorRelay<[Account]> { get }
    
    func updateAccounts()
    
    func persist(entities: [AccountEntity], finished:@escaping AccountsClosure)

    func loadAccountAsync(_ pageNumber: Int, finished: @escaping ([Account]) -> Void)
    func loadAllAccountsAsync(finished: ([Account]) -> Void)
}

class DataInteraction: DataInteractor {

    var accountsRelay = BehaviorRelay(value: [Account]())
    var currentDataLayer = CoreDataLayer()
    
    init() {
        currentDataLayer.setupCoreData()
        
//        DispatchQueue.global().asyncAfter(deadline: .now() + 3) {
//            self.updateAccounts()
//        }
    }
    
    func updateAccounts() {
        
        currentDataLayer.updateAccountThroughChildContext { [weak self] in
            guard let strongSelf = self else { return }
            
            print(strongSelf.accountsRelay.value)
            let accounts = strongSelf.currentDataLayer.loadAccount(0)
            print(accounts[0].institution)
        }
        
        currentDataLayer.fakeImportFromOtherServer { [weak self] in
            guard let strongSelf = self else { return }
            
            let accounts = strongSelf.currentDataLayer.loadAllAccounts()
            strongSelf.accountsRelay.accept(accounts)
            strongSelf.accountsRelay.value.forEach { print("\($0.nickname)") }
        }
    }
    
    func persist(entities: [AccountEntity], finished: @escaping AccountsClosure) {
        currentDataLayer.persist(entities: entities, finished: finished)
    }
    
    func loadAccountAsync(_ pageNumber: Int, finished: @escaping AccountsClosure) {
        currentDataLayer.loadAccountAsync(pageNumber, finished: finished)
    }
    
    func loadAllAccountsAsync(finished: AccountsClosure) {
        currentDataLayer.loadAllAccountsAsync(finished: finished)
    }
}

