//
//

import Foundation
import CoreData


extension Account {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Account> {
        return NSFetchRequest<Account>(entityName: "Account")
    }

    @NSManaged public var currency: String
    @NSManaged public var currentBalance: NSDecimalNumber
    @NSManaged public var currentBalanceInBase: NSDecimalNumber
    @NSManaged public var id: Int64
    @NSManaged public var institution: String
    @NSManaged public var nickname: String

}
