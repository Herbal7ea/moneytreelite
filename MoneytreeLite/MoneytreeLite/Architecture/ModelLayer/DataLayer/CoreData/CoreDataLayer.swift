import Foundation
import CoreData

typealias AccountsClosure = ([Account]) -> Void
typealias VoidBlock = () -> Void

class CoreDataLayer {
    
    let numberOfAccountsCreated = 3
    let pageSize = 3
    
    let parentContext = CoreDataStack.sharedInstance.persistentContainer.viewContext
    let persistentContainer = CoreDataStack.sharedInstance.persistentContainer
    
    var mergeNotificationObserver: NSObjectProtocol?
    
    lazy var numberOfResults: Int = {
        let fetchRequest: NSFetchRequest<Account> = Account.fetchRequest()
        
        let count = try! self.parentContext.count(for: fetchRequest)
        
        return count
    }()
    
    func resetDatabase() {
        CoreDataStack.sharedInstance.clearOldResults()
    }
    
    func setupCoreData() {
        
        resetDatabase()
        
        var accounts = [Account]()
        
        for i in 1 ... numberOfAccountsCreated {
            let account = Account(context: parentContext)
                account.id = Int64(i)
                account.nickname = "Name \(i)"
                account.institution = "Institution \(i)"
                account.currency = "USD"
                account.currentBalance = Decimal(integerLiteral: i) as NSDecimalNumber
                account.currentBalanceInBase = Decimal(integerLiteral: i) as NSDecimalNumber
           
            accounts.append(account)
        }
        
        try! parentContext.save()
    }
    
    func loadAccount(_ pageNumber: Int, context: NSManagedObjectContext? = nil) -> [Account] {
        
        let offset = pageSize * pageNumber
        let sort = NSSortDescriptor(key:"id", ascending: true)
        
        let fetchRequest: NSFetchRequest<Account> = Account.fetchRequest()
            fetchRequest.sortDescriptors = [sort]
            fetchRequest.fetchLimit  = pageSize
            fetchRequest.fetchOffset = offset
        
        let currentContext = context ?? self.parentContext
        let accounts = try! currentContext.fetch(fetchRequest)
        
        return accounts
    }

    
    func loadAccountAsync(_ pageNumber: Int, context: NSManagedObjectContext? = nil, finished: @escaping ([Account]) -> Void) {
        let accounts = loadAccount(pageNumber, context: context)
        finished(accounts)
    }
            
    func loadAllAccountsAsync(context: NSManagedObjectContext? = nil, finished: ([Account]) -> Void) {
        let accounts = loadAllAccounts(context: context)
        finished(accounts)
    }

    
    func loadAllAccounts(context: NSManagedObjectContext? = nil) -> [Account] {
        
        let sort = NSSortDescriptor(key:"id", ascending: true)
        
        let fetchRequest: NSFetchRequest<Account> = Account.fetchRequest()
            fetchRequest.sortDescriptors = [sort]
        
        let currentContext = context ?? self.parentContext
        let accounts = try! currentContext.fetch(fetchRequest)
        
        return accounts
    }
    
}

// Children Contexts example
extension CoreDataLayer {
    
    func fakeImportFromOtherServer(finish:@escaping () -> Void) {
        
        //fake server delay
        Thread.sleep(forTimeInterval: 1)
        
        //act like we are parsing and creating new objects
        self.persistentContainer.performBackgroundTask{ context in
            var accounts = [Account]()
            let startIndex = self.numberOfAccountsCreated + 1
            let endIndex   = (2 * self.numberOfAccountsCreated)
            
            for i in startIndex ... endIndex {
                
                let account = Account(context: context)
                    account.id = Int64(i)
                    account.nickname = "Name \(i)"
                    account.institution = "2nd Coordinator - Account \(i)"
                    account.currency = "USD"
                    account.currentBalance = Decimal(integerLiteral: i) as NSDecimalNumber
                    account.currentBalanceInBase = Decimal(integerLiteral: i) as NSDecimalNumber
                
                accounts.append(account)
            }
            
            try! context.save()
            
            DispatchQueue.main.async {
                finish()
            }
        }
    }
    
    func updateAccountThroughChildContext(finish:@escaping () -> Void ) {
        persistentContainer.performBackgroundTask{ context in

            let fetchRequest: NSFetchRequest<Account> = Account.fetchRequest()

            let accounts = try! context.fetch(fetchRequest)

            accounts.forEach { $0.nickname = "1st Coordinator - " + $0.nickname }

            context.trySave()

            DispatchQueue.main.async {
                finish()
            }
        }
    }
    
    func persist(entities: [AccountEntity], finished:@escaping AccountsClosure) {

        persistentContainer.performBackgroundTask{ context in
            
            //Normally I would sync records, but this is fake data, so we'll just clear all the old data out
            CoreDataStack.sharedInstance.clearAccounts(context)
            
            let accounts = entities.map { Account(entity: $0, context: context) }
            
            try! context.save()
            
            DispatchQueue.main.async {
                self.loadAllAccountsAsync(finished: finished)
            }
        }
    }

        
        
        
        
        
        
        
//    func getMessagesOnMainThread() -> [Message] {
//
//        let descriptor = NSSortDescriptor(key: #keyPath(Message.id), ascending: true)
//
//        let request: NSFetchRequest<Message> = Message.fetchRequest()
//        request.sortDescriptors = [descriptor]
//
//        //on main thread
//        let messages = try! persistentContainer.viewContext.fetch(request)
//
//        //        messages.forEach { print($0.title) }
//
//
//
//        return messages
//    }
}
