import Foundation

protocol AccountsGrouper {
    func institutionsStortedFor(groupAccounts: [String: [Account]]) -> [String]
    func groupAccountsByInstitution(_ accounts: [Account]) -> [String: [Account]]
}

class AccountsGroupingHelper: AccountsGrouper { //🐟
    func institutionsStortedFor(groupAccounts: [String: [Account]]) -> [String] {
        return Array(groupAccounts.keys).sorted(by: <)
    }
    
    func groupAccountsByInstitution(_ accounts: [Account]) -> [String: [Account]] {
        var accountsByInstitution = [String: [Account]]()
        let groupAccounts = Dictionary.init(grouping: accounts, by: {$0.institution})
        
        //feel like there must be some more elegant way to do this
        groupAccounts.forEach { institution, accounts in
            let sortedAccounts = accounts.sorted{ $0.nickname < $1.nickname }
            accountsByInstitution[institution] = sortedAccounts
        }
        
        return accountsByInstitution
    }
}
