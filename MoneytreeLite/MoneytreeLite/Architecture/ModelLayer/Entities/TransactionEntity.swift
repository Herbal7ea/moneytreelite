import Foundation

struct TransactionEntity: Codable {
    var id: Int
    var date: String //"2017-08-22T00:00:00+09:00",
    var amount: Decimal
    var accountId: Int
    var categoryId: Int
    var description: String //"\u53d6\u5f15\u660e\u7d30\u540d 2017-08-22T17:19:31+09:00",
}

struct TransactionResultWrapper: Codable {
    var transactions: [TransactionEntity]
}
