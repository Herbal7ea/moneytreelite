import Foundation
import CoreData

struct AccountEntity: Codable {
    var id: Int
    var nickname: String
    var institution: String
    var currency: String
    var currentBalance: Decimal
    var currentBalanceInBase: Decimal
}

struct AccountsResultWrapper: Codable {
    var accounts: [AccountEntity]
}

extension Account {
    convenience init(entity: AccountEntity, context: NSManagedObjectContext) {
        self.init(context: context)
            id = Int64(entity.id)
            nickname = entity.nickname
            institution = entity.institution
            currency = entity.currency
            currentBalance = entity.currentBalance as NSDecimalNumber
            currentBalanceInBase = entity.currentBalanceInBase  as NSDecimalNumber
    }
}
