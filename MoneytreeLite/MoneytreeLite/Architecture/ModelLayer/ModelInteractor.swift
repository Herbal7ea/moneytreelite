import Foundation
import RxSwift
import RxRelay

typealias AccountsResultClosure = (Result<[AccountEntity], NetworkError>)-> Void
typealias TransactionsResultClosure = (Result<[TransactionEntity], NetworkError>)-> Void

protocol ModelInteractor {
    var accountsRelay: BehaviorRelay<[Account]> { get }
    var modelErrorRelay: PublishRelay<NetworkError> { get }

    func loadAccounts(finished: @escaping AccountsResultClosure)
    func loadTransactions(id: Int, finished: @escaping TransactionsResultClosure)
}




class ModelInteraction: ModelInteractor {
    private var networkLayer: NetworkInteractor
    private var dataLayer: DataInteractor
    var accountsRelay: BehaviorRelay<[Account]>
    var modelErrorRelay = PublishRelay<NetworkError>() // given time, I would make a Model Error class that maps to the network and data layer Errors classes
    
    init(networkInteractor: NetworkInteractor, dataInteractor: DataInteractor) {
        self.networkLayer = networkInteractor
        self.dataLayer = dataInteractor
        accountsRelay = dataLayer.accountsRelay
        
        getAccounts()
    }
    
    func loadAccounts(finished: @escaping AccountsResultClosure) {
        networkLayer.loadAccounts(finished: finished)
    }
    
    func loadTransactions(id: Int, finished: @escaping TransactionsResultClosure) {
        networkLayer.loadTransactions(id: id, finished: finished)
    }
    
    private func persist(entities: [AccountEntity], finished:@escaping AccountsClosure) {
        dataLayer.persist(entities: entities, finished: finished)
    }
    
    //All of the complexity (local db call, network call, conversion to Core Data,
    //loading of local results again, are all removed from presentation layer because we
    //are using the RxRelay to simplify the public UI - subscription is expected
    //to be called multiple times.
    func getAccounts() {
        //load local data
        //model interactor lasts the life of the app, so just `self` is safe to use here
        //All threading is transparent to this layer but - Main Thread here
        dataLayer.loadAllAccountsAsync { localAccounts in
            print("🐴 loaded local data: \(localAccounts[0].nickname)")
            self.accountsRelay.accept(localAccounts)
            
            //load Network Data
            self.loadNetworkAccounts()
        }
    }


    func loadNetworkAccounts() {
        //BG Thread hear
        networkLayer.loadAccounts { result in
            switch result {
            case .success(let accountEntities):
                
                print("🦄 loaded network data: \(accountEntities[0].nickname)")
                
                //convert and save to local db
                //BG thread here
                self.dataLayer.persist(entities: accountEntities) { newLocalAccounts in
                    print("🍄 loaded network data: \(newLocalAccounts.first?.nickname)")
                    //update again with latest from network
                    //normally I would use a time stamp to diff what I have loaded vs. what is new from the server.
                    //Back to main Thread in this closure
                    self.accountsRelay.accept(newLocalAccounts)                    }
            case .failure(let error):
                //notify user of error
                //TODO: add UI to present message to user
                self.modelErrorRelay.accept(.requestError(message: error.localizedDescription))
            }
        }
    }
    
//    func loadAccountAsync(_ pageNumber: Int, finished: @escaping ([Account]) -> Void) {
//        dataLayer.loadAccountAsync(pageNumber, finished: finished)
//    }
//
//    func loadAllAccountsAsync(finished: ([Account]) -> Void) {
//        dataLayer.loadAllAccountsAsync(finished: finished)
//    }

}
