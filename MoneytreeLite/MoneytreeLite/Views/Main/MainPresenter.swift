import Foundation
import RxSwift
import RxRelay

class MainPresenter {
    
    let accountsByInstitutionRelay = BehaviorRelay<Dictionary<String, [Account]>>(value: [:])
    
    var numberOfSections: Int { return institutions.count }
    var bag = DisposeBag()

    private let interactor: ModelInteractor
    private let accountsGrouper: AccountsGrouper
    private var institutions = [String]()
    
    init(interactor: ModelInteractor, accountsGrouper: AccountsGrouper) {
        self.interactor = interactor
        self.accountsGrouper = accountsGrouper
        
        loadAccounts()
    }
    
    func loadAccounts() {
        interactor.accountsRelay.subscribe(onNext: { [weak self] accounts in
            print("🏎 accountsRelay: \(accounts.first?.nickname)")
            self?.handleUpdates(for: accounts)
        }).disposed(by: bag)
            
        interactor.modelErrorRelay.subscribe(onNext: { [weak self] error in
            self?.notifyOfError(error)
        }).disposed(by: bag)
    }
    
    
    
    //public for testing
    //TODO: jbott - 06.17.19 - adds a warning - must be some more elegant way to fix this
    @available(*, deprecated, message: "This method should not be used outside of this class, it is public for testing purposes only" )
    func handleUpdates(for accounts: [Account]) {
        let accountsByInstitution = accountsGrouper.groupAccountsByInstitution(accounts)
        institutions = accountsGrouper.institutionsStortedFor(groupAccounts: accountsByInstitution)
        accountsByInstitutionRelay.accept(accountsByInstitution)
    }
    
    func notifyOfError(_ error: Error){
        print("❗️🧙🏻‍♂️❗️ You shall not pass!: \(error)")
    }
    
    func numberOfRowsIn(section: Int) -> Int {
        return accountsIn(section: section)?.count ?? 0
    }
    
    func accountsIn(section: Int) -> [Account]? {
        let sectionName = institutions[section]
        return accountsByInstitutionRelay.value[sectionName]
    }
    
    func institutionNameFor(section: Int) -> String {
        return institutions[section]
    }
    
    func accountFor(indexPath: IndexPath) -> Account? {
        let institution = institutions[indexPath.section]
        let account = accountsByInstitutionRelay.value[institution]?[indexPath.row]
        return account
    }
}
