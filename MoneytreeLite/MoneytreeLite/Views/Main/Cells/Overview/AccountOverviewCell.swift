//

import UIKit

class AccountOverviewCell: UITableViewCell {

    @IBOutlet var accountLabel: UILabel!
    @IBOutlet var balanceLabel: UILabel!
    
    private var account: Account?
    
    var balance: String {
        guard let account = account,
            let locale = account.currency.asCurrency?.locale
            else { return "" }
        let d = account.currentBalance as Decimal
        return "\(account.currency) \(d.convertToCurrency(for: locale))"
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        account = nil
        accountLabel.text = ""
        balanceLabel.text = ""
    }
    
    func prepare(with account: Account?) {
        self.account = account
        
        //TODO: jbott - 06/16/18 - determing if this value changes, inject values
        if let account = account {
            accountLabel.text = account.nickname
            balanceLabel.text = balance
        }
    }
}

// MARK: - cell creation methods
extension AccountOverviewCell {
    static var identifier = "\(String(describing: self))"
    static var nib: UINib {
        return UINib(nibName: "\(String(describing: self))", bundle: nil)
    }
}

private extension Decimal {
    func convertToCurrency(for locale: Locale) -> String {
        let currencyFormatter = NumberFormatter()
            currencyFormatter.numberStyle = .currency
            currencyFormatter.usesGroupingSeparator = true
            currencyFormatter.locale = locale
            // localing how we group and what decimal we use
        let number = self as NSNumber
        let priceString = currencyFormatter.string(from: number) ?? "\(self)"
        return priceString
    }
}

enum Currency: String {
    
    case jpy,
         usd

    
    //This could be WAY better.  Hacking locale stuff because of time constraint
    var locale: Locale? {
        switch self {
        case .jpy:
            return Locale(identifier: "ja_JP")
        case .usd:
            return Locale(identifier: "en_US")
        }
    }
    
    init?(type: String) {
        switch type {
        case Currency.jpy.rawValue.uppercased(): self = .jpy
        case Currency.usd.rawValue.uppercased(): self = .usd
        default: return nil
        }
    }
}

private extension String {
    var asCurrency: Currency? { return Currency(type: self) }
}
