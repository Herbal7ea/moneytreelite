//

import XCTest
import InstantMock
@testable import MoneytreeLite

class MainPresenterTests: XCTestCase {

    private var classUnderTest: MainPresenter!
    private var mockModelInteractor = MockModelInteractor()
    private var mockAccountsGrouper = MockAccountsGrouper()
    override func setUp() {
        classUnderTest = MainPresenter(interactor: mockModelInteractor, accountsGrouper: mockAccountsGrouper)
    }

    override func tearDown() {
        mockAccountsGrouper.resetExpectations()
        mockModelInteractor.resetExpectations()
    }
    
    //using mocks to reduce changes for sideaffects
    func test_handleUpdates_usesAccountsGrouper() {
        //Arrange
        let accounts = AccountsTestBuilder.create()
        let groupedAccounts = ["fake": accounts]
        let institutions = ["A", "B", "C"]
        
        mockAccountsGrouper.expect().call(
            mockAccountsGrouper.groupAccountsByInstitution(Arg.any()) //Arg.eq(accounts))  //I would like to use more sepecific requirements, but internally the accounts doens't match up to what was mocked.  The test still has value, but I'd like to be stricter in my unit tests for high risk areas
            ).andReturn(groupedAccounts)
        
        mockAccountsGrouper.expect().call(
            mockAccountsGrouper.institutionsStortedFor(groupAccounts: Arg.any()) //Arg.eq(groupedAccounts)
            ).andReturn(institutions)
        
        //Act
        classUnderTest.handleUpdates(for: accounts)
        
        //Assert
        mockAccountsGrouper.verify()
    }
    
    
    func test_handleUpdates_updatesAccountsByInstitutionRelay() {
        //Arrange
        let accounts = AccountsTestBuilder.create()
        let groupedAccounts = ["fake": accounts]
        let institutions = ["A", "B", "C"]
        
        XCTAssertEqual([String: [AccountEntity]](), classUnderTest.accountsByInstitutionRelay.value)
        
        
        mockAccountsGrouper.stub().call(
            mockAccountsGrouper.groupAccountsByInstitution(Arg.any()) //Arg.eq(accounts))  //I would like to use more sepecific requirements, but internally the accounts doens't match up to what was mocked.  The test still has value, but I'd like to be stricter in my unit tests for high risk areas
        ).andReturn(groupedAccounts)
        
        mockAccountsGrouper.stub().call(
            mockAccountsGrouper.institutionsStortedFor(groupAccounts: Arg.any()) //Arg.eq(groupedAccounts)
        ).andReturn(institutions)
        
        //Act
        classUnderTest.handleUpdates(for: accounts)
        
        //Assert
        XCTAssertEqual(groupedAccounts, classUnderTest.accountsByInstitutionRelay.value)
    }

}

// MARK: Mock class inherits from `Mock` and adopts the `Foo` protocol
class MockModelInteractor: Mock, ModelInteractor {
    func loadAccounts(finished: @escaping AccountsResultClosure) {
        super.call(finished)
    }
    
    func loadTransactions(id: Int, finished: @escaping TransactionsResultClosure) {
        super.call(id, finished)
    }
}

class MockAccountsGrouper: Mock, AccountsGrouper {
    func institutionsStortedFor(groupAccounts: [String: [AccountEntity]]) -> [String] {
        return super.call(groupAccounts)!
    }
    
    func groupAccountsByInstitution(_ accounts: [AccountEntity]) -> [String: [AccountEntity]] {
        return super.call(accounts)!
    }
}

