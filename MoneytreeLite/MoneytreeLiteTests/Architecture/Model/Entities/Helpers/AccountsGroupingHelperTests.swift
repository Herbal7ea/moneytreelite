//

import XCTest
@testable import MoneytreeLite

class AccountsGroupingHelperTests: XCTestCase {

    private var classUnderTest: AccountsGrouper = AccountsGroupingHelper()
    
    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
}

//// MARK: - tests for: institutionsStortedFor
//extension AccountsGroupingHelperTests {
//    /*
//    typically test for:
//        • Normal Result
//        • Empty Arrays / Dict
//        • Nil, if possible
//        • Outlier examples
//    */
//
//    func test_institutionsStortedFor_whenStandardData_returnSortedKeys() {
//        //Arrange
//        let groupAccounts: [String: [AccountEntity]] = ["C": [], "D": [], "A": [], "B": [] ]
//        let expected = ["A", "B", "C", "D"]
//
//        //Act
//        let result = classUnderTest.institutionsStortedFor(groupAccounts: groupAccounts)
//
//        //Assert
//        XCTAssertEqual(expected, result)
//    }
//
//    func test_institutionsStortedFor_whenEmptyDict_returnEmptyArray() {
//        //Arrange
//        let groupAccounts = [String: [AccountEntity]]()
//        let expected = [String]()
//
//        //Act
//        let result = classUnderTest.institutionsStortedFor(groupAccounts: groupAccounts)
//
//        //Assert
//        XCTAssertEqual(expected, result)
//    }
//}
//
//// MARK: - tests for: groupAccountsByInstitution
//extension AccountsGroupingHelperTests {
//    /*
//
//     If I have not done this TDD, I would look over the function
//     and determine what needs to be tested
//
//
//     Things to test:
//        Did it group the way I expected
//        Are the Accounts ordered by nickname
//        what happens if the main accounts array is empty
//
//    Then I draw up the tests and fill them in
//
//
//    Method for drawing up notes - usually delete this after determining what tests are good.
//    func groupAccountsByInstitution(_ accounts: [Account]) -> [String: [Account]] {
//        var accountsByInstitution = [String: [Account]]()
//        let groupAccounts = Dictionary.init(grouping: accounts, by: {$0.institution})
//
//        //feel like there must be some more elegant way to do this
//        groupAccounts.forEach { institution, accounts in
//            let sortedAccounts = accounts.sorted{ $0.nickname > $1.nickname }
//            accountsByInstitution[institution] = sortedAccounts
//        }
//
//        return accountsByInstitution
//    }
//
//     */
//
//    // Did it group the way I expected
//    func test_groupAccountsByInstitution_groupsAsExpected() {
//
//        //Arrange
//        let accounts = AccountsTestBuilder.create()
//
//        let key1 = "Test Bank"
//        let key2 = "Starbucks Card"
//
//        //Act
//        let result = classUnderTest.groupAccountsByInstitution(accounts)
//
//        //Assert
//        XCTAssertTrue(result[key1]!.contains(accounts[0]))
//        XCTAssertTrue(result[key2]!.contains(accounts[2]))
//        XCTAssertTrue(result[key2]!.contains(accounts[3]))
//        XCTAssertTrue(result[key2]!.contains(accounts[1]))
//    }
//
//    // Are the Accounts ordered by nickname
//    func test_groupAccountsByInstitution_accountsOrderedByNickname() {
//        //Arrange
//        let accounts = AccountsTestBuilder.create()
//
//        let key1 = "Test Bank"
//        let key2 = "Starbucks Card"
//        let expectedAccounts1 = [accounts[0]]
//        let expectedAccounts2 = [accounts[2], accounts[3], accounts[1]]
//
//        //Act
//        let result = classUnderTest.groupAccountsByInstitution(accounts)
//
//        //Assert
//        XCTAssertEqual(expectedAccounts1, result[key1]!)
//        XCTAssertEqual(expectedAccounts2, result[key2]!)
//    }
//
//    // what happens if the main accounts array is empty
//    func test_groupAccountsByInstitution_whenAccountsIsEmpty_returnEmptyDictionary() {
//        //Arrange
//        let accounts = [AccountEntity]()
//        let expected = [String: [AccountEntity]]()
//
//        //Act
//        let result = classUnderTest.groupAccountsByInstitution(accounts)
//
//        //Assert
//        XCTAssertEqual(expected, result)
//    }
//}
//
//extension AccountEntity: Equatable {
//    public static func == (lhs: AccountEntity, rhs: AccountEntity) -> Bool {
//        return lhs.id == rhs.id &&
//               lhs.nickname == rhs.nickname &&
//               lhs.institution == rhs.institution &&
//               lhs.currency == rhs.currency &&
//               lhs.currentBalance == rhs.currentBalance &&
//               lhs.currentBalanceInBase == rhs.currentBalanceInBase
//    }
//}
//
//class AccountsTestBuilder {
//    static func create() -> [AccountEntity] {
//        let accounts = [
//        AccountEntity(id: 1,
//                nickname: "外貨普通(USD)",
//                institution: "Test Bank",
//                currency: "USD",
//                currentBalance: 22.5,
//                currentBalanceInBase: 2306.0),
//
//        AccountEntity(id: 2,
//                nickname: "ccccccc",
//                institution: "Starbucks Card",
//                currency: "JPY",
//                currentBalance: 3035.0,
//                currentBalanceInBase: 3035.0),
//
//        AccountEntity(id: 3,
//                nickname: "aaaaaaaa",
//                institution: "Starbucks Card",
//                currency: "JPY",
//                currentBalance: 0.0,
//                currentBalanceInBase: 0.0),
//
//        AccountEntity(id: 4,
//                nickname: "bbbbbbb",
//                institution: "Starbucks Card",
//                currency: "JPY",
//                currentBalance: 0.0,
//                currentBalanceInBase: 0.0)]
//
//        return accounts
//    }
//}
